#[derive(Debug, Copy, Clone)]
pub struct Cell {
    pub visited: bool,
    pub walls: [bool; 4], // top, right, bottom, left
}

impl Cell {
    pub fn new() -> Self {
        Cell {
            visited: false,
            walls: [true; 4],
        }
    }
}
