use std::cmp::{Eq, Ord};

use crate::direction::Direction;

#[derive(Debug, Copy, Clone)]
pub struct Vector2<T> {
    pub x: T,
    pub y: T,
}

impl<T: Eq + Ord> Vector2<T> {
    pub fn new(x: T, y: T) -> Self {
        Vector2 { x, y }
    }

    pub fn get_direction(v0: &Vector2<T>, v1: &Vector2<T>) -> Option<Direction> {
        if v0.x == v1.x && v0.y == v1.y {
            None
        } else if v0.x == v1.x {
            // top or bottom
            if v0.y > v1.y { Some(Direction::Top) }
            else { Some(Direction::Bottom) }
        } else {
            // left or right
            if v0.x > v1.x { Some(Direction::Left) }
            else { Some(Direction::Right) }
        }
    }
}