use image::{ImageBuffer, Rgb, imageops};
use rand::{thread_rng, Rng};

use crate::cell::Cell;
use crate::vector2::Vector2;
use crate::direction::Direction;

const IMAGE_SIZE_MULTIPLIER: u32 = 10;

#[derive(Debug)]
pub struct Maze {
    pub cells: Vec<Vec<Cell>>,
    width: u32,
    height: u32,
    wall_color: Rgb<u8>,
    path_color: Rgb<u8>,
    step: u32,
    current_cell: Vector2<usize>,
}

impl Maze {
    pub fn new(width: u32, height: u32, wall_color: Rgb<u8>, path_color: Rgb<u8>) -> Self {
        Maze {
            cells: vec![vec![Cell::new(); height as usize]; width as usize],
            width,
            height,
            wall_color,
            path_color,
            step: 0,
            current_cell: Vector2::new(0, 0),
        }
    }

    pub fn generate(&mut self, output_steps: bool) {
        let mut traversal_stack: Vec<Vector2<usize>> = Vec::new();
        let mut rng = thread_rng();

        // start off by pushing our start node onto the stack
        traversal_stack.push(Vector2::new(0, 0));
        self.cells[0][0].visited = true;

        let mut available_cells: Vec<Vector2<usize>> = Vec::with_capacity(4);
        // loop while we still have nodes to visit
        while traversal_stack.len() != 0 {
            self.step += 1;
            // get the available directions
            available_cells.clear();
            let v0: Vector2<usize> = match traversal_stack.last() {
                Some(value) => value,
                None        => panic!("traversal stack was empty while trying to move"),
            }.clone();
            if output_steps {
                self.current_cell = v0;
                self.render(format!("step_{}", self.step).as_str(), true);
            }

            if v0.x != 0 {
                if self.check_valid_coord(v0.x - 1, v0.y) {
                    available_cells.push(Vector2::new(v0.x - 1, v0.y))
                }
            }
            if self.check_valid_coord(v0.x + 1, v0.y) {
                available_cells.push(Vector2::new(v0.x + 1, v0.y))
            }
            if v0.y != 0 {
                if self.check_valid_coord(v0.x, v0.y - 1) {
                    available_cells.push(Vector2::new(v0.x, v0.y - 1))
                }
            }
            if self.check_valid_coord(v0.x, v0.y + 1) {
                available_cells.push(Vector2::new(v0.x, v0.y + 1))
            }

            // if there's nowhere to go then pop the current cell off the stack
            if available_cells.len() == 0 {
                traversal_stack.pop();
                continue;
            }

            // randomly pick where to go...
            let index = rng.gen_range(0, available_cells.len());
            let v1: Vector2<usize> = available_cells[index];
            // ...push it to the stack and visit it...
            traversal_stack.push(v1);
            self.cells[v1.x][v1.y].visited = true;
            // ...and finally break the walls between to the two cells
            let dir = match Vector2::get_direction(&v0, &v1) {
                Some(value) => value,
                None        => {
                    println!("failed to check direction");
                    continue
                },
            };
            self.cells[v0.x][v0.y].walls[dir as usize] = false;
            self.cells[v1.x][v1.y].walls[(dir as usize + 2) % 4] = false;
        }

        // now that we're done generating the maze, add a start and end
        self.cells[0][0].walls[Direction::Left as usize] = false;
        let max_x: usize = self.width as usize - 1;
        let max_y: usize = self.height as usize - 1;
        self.cells[max_x][max_y].walls[Direction::Right as usize] = false;
    }

    fn check_valid_coord(&self, x: usize, y: usize) -> bool {
        // is within ranges
        if x >= self.width as usize || y >= self.height as usize {
            return false
        }

        // if cell is not visited
        return !self.cells[x][y].visited
    }

    pub fn render(&self, filename: &str, show_path: bool) {
        // setup
        let img_width: u32 = self.width * 2 + 1;
        let img_height: u32 = self.height * 2 + 1;
        let mut img_buf: ImageBuffer<Rgb<u8>, Vec<u8>> = ImageBuffer::new(
            img_width,
            img_height);

        // parsing/drawing
        for x in 0..self.cells.len() {
            for y in 0..self.cells[x].len() {
                let x_offset: u32 = x as u32 * 2;
                let y_offset: u32 = y as u32 * 2;

                // top left
                *img_buf.get_pixel_mut(x_offset, y_offset) = self.wall_color;
                // top
                if self.cells[x][y].walls[Direction::Top as usize] {
                    *img_buf.get_pixel_mut(x_offset + 1, y_offset) = self.wall_color;
                } else {
                    *img_buf.get_pixel_mut(x_offset + 1, y_offset) = self.path_color;
                }
                // left
                if self.cells[x][y].walls[Direction::Left as usize] {
                    *img_buf.get_pixel_mut(x_offset, y_offset + 1) = self.wall_color;
                } else {
                    *img_buf.get_pixel_mut(x_offset, y_offset + 1) = self.path_color;
                }
                // center
                if self.cells[x][y].visited {
                    if self.current_cell.x == x && self.current_cell.y == y && show_path {
                        *img_buf.get_pixel_mut(x_offset + 1, y_offset + 1) = Rgb::from([255, 0, 0]);
                    } else {
                        *img_buf.get_pixel_mut(x_offset + 1, y_offset + 1) = self.path_color;
                    }
                } else {
                    *img_buf.get_pixel_mut(x_offset + 1, y_offset + 1) = self.wall_color;
                }
            }

            // since we skip drawing the bottom of the cells, fill in the bottom of the last column
            let x_offset: u32 = x as u32 * 2;
            let y: usize = self.cells[x].len() - 1;
            let y_offset: u32 = y as u32 * 2;
            // bottom left
            *img_buf.get_pixel_mut(x_offset, y_offset + 2) = self.wall_color;
            // bottom
            if self.cells[x][y].walls[Direction::Bottom as usize] {
                *img_buf.get_pixel_mut(x_offset + 1, y_offset + 2) = self.wall_color;
            } else {
                *img_buf.get_pixel_mut(x_offset + 1, y_offset + 2) = self.path_color;
            }
        }

        // since we skip drawing the right of the cells, fill in the right of the last row
        let x: usize = self.cells.len() - 1;
        let x_offset: u32 = x as u32 * 2;
        for y in 0..self.cells[x].len() {
            let y_offset: u32 =  y as u32 * 2;
            // top right
            *img_buf.get_pixel_mut(x_offset + 2, y_offset) = self.wall_color;
            // right
            if self.cells[x][y].walls[Direction::Right as usize] {
                *img_buf.get_pixel_mut(x_offset + 2, y_offset + 1) = self.wall_color;
            } else {
                *img_buf.get_pixel_mut(x_offset + 2, y_offset + 1) = self.path_color;
            }
        }

        // finally, fill in the bottom right most pixel
        *img_buf.get_pixel_mut(img_height - 1, img_width - 1) = self.wall_color;

        // scaling
        let new_width: u32 = img_width * IMAGE_SIZE_MULTIPLIER;
        let new_height: u32 = img_height * IMAGE_SIZE_MULTIPLIER;
        img_buf = imageops::resize(&mut img_buf, new_width, new_height, imageops::Nearest);

        // saving
        img_buf.save(format!("{}.png", filename)).expect("failed to save image");
    }
}