pub mod cell;
pub mod maze;
pub mod vector2;
pub mod direction;

use maze::Maze;
use image::Rgb;

const MAZE_WIDTH: u32 = 50;
const MAZE_HEIGHT: u32 = 50;

fn main() {
    println!("creating maze instance");
    let mut maze = Maze::new(MAZE_WIDTH, MAZE_HEIGHT, Rgb::from([0, 0, 0]), Rgb::from([255, 255, 255]));
    println!("generating maze");
    maze.generate(false);

    println!("saving maze image");
    maze.render("final_output", false);
    println!("done!");
}
